include(rgg.bundle.common)
set(CPACK_MONOLITHIC_INSTALL TRUE)

set(library_paths "lib")

if (Qt5_DIR)
  list(APPEND library_paths
    "${Qt5_DIR}/../../../bin")
endif ()

superbuild_windows_install_program("RGGNuclear"
  "bin"
  SEARCH_DIRECTORIES "${library_paths}")

foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  superbuild_windows_install_plugin(
    "${qt5_plugin_path}"
    "bin/${qt5_plugin_group}"
    SEARCH_DIRECTORIES "${library_paths}")
endforeach ()

if (meshkit_enabled)
  foreach (meshkit_exe IN ITEMS coregen assygen)
    _superbuild_windows_install_binary(
      BINARY    "${superbuild_install_location}/meshkit/bin/${meshkit_exe}.exe"
      LIBDIR    "meshkit/bin"
      TYPE      module
      LOCATION  "meshkit/bin"
      BINARY_LIBDIR "meshkit/bin"
      SEARCH_DIRECTORIES "${superbuild_install_location}/meshkit/bin")
  endforeach ()
endif ()
