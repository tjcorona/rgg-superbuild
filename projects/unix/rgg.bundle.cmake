include(rgg.bundle.common)
set(rgg_package "RGG_Suite_${rgg_version_major}.${rgg_version_minor}.${rgg_version_patch}")

set(library_paths
  "${superbuild_install_location}/lib")

if (Qt5_DIR)
  list(APPEND library_paths
    "${Qt5_DIR}/../..")
endif ()

# RGGNuclear
superbuild_unix_install_program_fwd("RGGNuclear"
  "lib"
  SEARCH_DIRECTORIES "${library_paths}")

# Qt
if (qt5_enabled)
  file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/qt.conf" "")
  install(
    FILES       "${CMAKE_CURRENT_BINARY_DIR}/qt.conf"
    DESTINATION "lib/cmb-${cmb_version}"
    COMPONENT   superbuild)
endif ()

foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  superbuild_unix_install_plugin("${qt5_plugin_path}"
    "lib"
    "lib/${qt5_plugin_group}/"
    LOADER_PATHS    "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endforeach ()

if (meshkit_enabled)
  foreach (meshkit_exe IN ITEMS coregen assygen)
    superbuild_unix_install_plugin("${superbuild_install_location}/meshkit/bin/${meshkit_exe}"
      "meshkit/lib"
      "meshkit/bin"
      SEARCH_DIRECTORIES "${superbuild_install_location}/meshkit/lib")
  endforeach ()
endif ()
