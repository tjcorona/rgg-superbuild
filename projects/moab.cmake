superbuild_add_project(moab
  DEPENDS cxx11 eigen netcdf
  CMAKE_ARGS
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=OFF
    -DENABLE_BLASLAPACK:BOOL=OFF
    -DMOAB_HAVE_EIGEN:BOOL=ON
    -DENABLE_IMESH:BOOL=ON
    -DMOAB_USE_SZIP:BOOL=ON
    -DMOAB_USE_CGM:BOOL=OFF
    -DMOAB_USE_CGNS:BOOL=OFF
    -DMOAB_USE_MPI:BOOL=OFF
    -DMOAB_USE_HDF:BOOL=OFF
    -DMOAB_USE_NETCDF:BOOL=ON
    -DENABLE_PNETCDF:BOOL=OFF
    -DENABLE_MPI:BOOL=OFF
    -DENABLE_TESTING:BOOL=OFF
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>
    -DMOAB_ENABLE_TESTING:BOOL=OFF)

superbuild_apply_patch(moab disable-fortran
  "Disable use of fortran")

superbuild_apply_patch(moab export-include-dir
  "Set MOAB and iMesh targets to export their installed include directories")

superbuild_apply_patch(moab broken-moab-config
  "Handle empty lists in MOABConfig.cmake")

# By default, linux and os x cmake looks in <INSTALL_DIR>/lib/cmake for
# things. On windows, it does not. So, we set MOAB_DIR to point to the
# location of MOABConfig.cmake for everyone.
superbuild_add_extra_cmake_args(
  -DMOAB_DIR:PATH=<INSTALL_DIR>/lib/cmake/MOAB)
