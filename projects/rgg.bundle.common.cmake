include(rgg-version)

set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "RGG suite")
set(CPACK_PACKAGE_NAME "rgg")
set(CPACK_PACKAGE_VENDOR "Kitware, Inc.")
set(CPACK_PACKAGE_VERSION_MAJOR "${rgg_version_major}")
set(CPACK_PACKAGE_VERSION_MINOR "${rgg_version_minor}")
set(CPACK_PACKAGE_VERSION_PATCH "${rgg_version_minor}")

set(CPACK_PACKAGE_FILE_NAME
  "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
set(CPACK_RESOURCE_FILE_LICENSE
  "${superbuild_source_directory}/License.txt")

include(qt5.functions)

set(qt5_plugin_prefix)
if (NOT WIN32)
  set(qt5_plugin_prefix "lib")
endif ()

set(qt5_plugins
  sqldrivers/${qt5_plugin_prefix}qsqlite)

if (WIN32)
  list(APPEND qt5_plugins
    platforms/qwindows)
elseif (APPLE)
  list(APPEND qt5_plugins
    platforms/libqcocoa
    printsupport/libcocoaprintersupport)
elseif (UNIX)
  list(APPEND qt5_plugins
    platforms/libqxcb
    platforminputcontexts/libcomposeplatforminputcontextplugin
    xcbglintegrations/libqxcb-glx-integration)
endif ()

superbuild_install_qt5_plugin_paths(qt5_plugin_paths ${qt5_plugins})
