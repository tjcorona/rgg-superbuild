superbuild_add_project(meshkit
  DEPENDS moab cgm
  CMAKE_ARGS
    -DCGM_DIR:PATH=<INSTALL_DIR>/lib/cmake/CGM
    -DBUILD_ALGS:BOOL=ON
    -DBUILD_SRC:BOOL=ON
    -DBUILD_UTILS:BOOL=ON
    -DBUILD_RGG:BOOL=ON
    -DWITH_MPI:BOOL=OFF
    -DENABLE_TESTING:BOOL=OFF)

superbuild_apply_patch(meshkit hugeval
  "Replace HUGE with HUGE_VAL")

# MOAB has public headers which include Eigen, but it doesn't add the include
# flags properly.
superbuild_append_flags(cxx_flags "-I<INSTALL_DIR>/include/eigen3" PROJECT_ONLY)
