superbuild_add_project(cgm
  CMAKE_ARGS
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DCGM_OCC:BOOL=OFF
    -DUSE_MPI:BOOL=OFF)

superbuild_add_extra_cmake_args(
  -DCGM_DIR:PATH=<INSTALL_DIR>/lib/cmake/CGM)
